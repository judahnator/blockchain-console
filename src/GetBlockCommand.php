<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetBlockCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('block:get')
            ->addArgument('hash', InputArgument::REQUIRED, 'The hash of the block')
            ->setDescription('Retrieves a block by its hash');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $block = Block::find($input->getArgument('hash'));
        }catch (InvalidBlockException $e) {
            $output->writeln('Block is invalid');
            return;
        }catch (BlockNotFoundException $e) {
            $output->writeln('Block not found');
            return;
        }

        if (property_exists($block->data, 'scalar')) {
            $data = $block->data->scalar;
        }else {
            $data = PHP_EOL.json_encode($block->data, JSON_PRETTY_PRINT);
        }

        $output->writeln([
            "Block {$block->hash}:",
            "Height: {$block->height}",
            "Data: {$data}",
            "Created At: {$block->created_at->format('m/d/Y H:i:s')}"
        ]);
    }
}