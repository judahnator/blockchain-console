<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteBlockCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('block:delete')
            ->setDescription('Deletes a given block')
            ->addArgument('hash', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $block = Block::find($input->getArgument('hash'));
        }catch (InvalidBlockException $e) {
            $output->writeln('Block is invalid');
            return;
        }catch (BlockNotFoundException $e) {
            $output->writeln('Block not found');
            return;
        }

        if ($block->height === 0) {
            $output->writeln('Cannot delete the origin block');
            return;
        }

        $block->delete();

        $output->writeln('Block deleted');
    }

}