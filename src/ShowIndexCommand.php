<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowIndexCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('index:show')
            ->setDescription('Displays the blockchain index');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach (BlockChain::load()->getFullIndex() as $height => $blocksAtHeight) {
            $output->writeln("Blocks at height {$height}:");
            foreach ($blocksAtHeight as $block) {
                /** @var Block $block */
                $output->writeln(" {$block->hash}");
            }
            $output->write(PHP_EOL);
        }
    }

}