<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportBlockCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('block:export')
            ->setDescription('Exports a JSON representation of a given block')
            ->addArgument('blockhash', InputArgument::REQUIRED)
            ->addOption('withChildren', '-c', InputOption::VALUE_NONE, 'Whether to include child blocks or not')
            ->addOption('recursiveChildren', '-r', InputOption::VALUE_NONE, 'Whether to include the childrens children');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $block = Block::find($input->getArgument('blockhash'));
        }catch (InvalidBlockException $e) {
            $output->writeln('Block is invalid');
            return;
        }catch (BlockNotFoundException $e) {
            $output->writeln('Block not found');
            return;
        }

        $output->writeln(
            json_encode(
                $block->export(
                    $input->getOption('withChildren'),
                    $input->getOption('recursiveChildren')
                ),
                JSON_PRETTY_PRINT
            )
        );
    }

}