<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateBlockCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('block:create')
            ->setDescription('Creates a new block')
            ->addArgument('parenthash', InputArgument::OPTIONAL)
            ->addArgument('blockdata', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->hasArgument('parenthash')) {
            $parentHash = $this
                ->getHelper('question')
                ->ask(
                    $input,
                    $output,
                    new Question(
                        'What is the hash of the parent block? (leave blank for the origin block)'.PHP_EOL,
                        BlockChain::originBlock()->hash
                    )
                );
        }else {
            $parentHash = $input->getArgument('parenthash');
        }

        try {
            $parentBlock = Block::find($parentHash);
        } catch (InvalidBlockException $e) {
            $output->writeln('The given block is invalid');
            return;
        } catch (BlockNotFoundException $e) {
            $output->writeln('The block could not be found');
            return;
        }

        if (!$input->hasArgument('blockdata')) {
            $blockData = $this
                ->getHelper('question')
                ->ask(
                    $input,
                    $output,
                    new Question('What data should this block contain?'.PHP_EOL)
                );
        } else {
            // Get the json
            $blockData = json_decode($input->getArgument('blockdata'));

            // json is invalid, use raw text instead
            if (json_last_error() !== JSON_ERROR_NONE) {
                $blockData = $input->getArgument('blockdata');
            }
        }

        $newBlock = Block::create(
            $parentBlock,
            (object)$blockData
        );

        $output->writeln("Block created with hash '{$newBlock->hash}'");
    }

}