<?php

namespace judahnator\BlockChainConsole;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RebuildIndexCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('index:rebuild')
            ->setDescription('Rebuilds the blockchain index');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        BlockChain::reindex();
        $output->writeln('Done reindexing');
    }

}